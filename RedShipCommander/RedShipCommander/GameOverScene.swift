//
//  GameOverScene.swift
//  RedShipCommander
//
//  Created by Erik Rubio on 1/14/17.
//  Copyright © 2017 Erik Rubio. All rights reserved.
//
import UIKit
import SpriteKit
import GameKit


class GameOverScene: SKScene, GKGameCenterControllerDelegate{

    var score:Int = 0
    
    var scoreLabel:SKLabelNode!
    
    var newGameButtonNode:SKSpriteNode!
    
    var gameCenterNode:SKSpriteNode!
    
    var starfield:SKEmitterNode!
    var canUseGameCenter:Bool = true {
        didSet {
            /* load prev. achievments form Game Center */
            if canUseGameCenter == true { gameCenterLoadAchievements() }
        }}
    /// Achievements of player
    var gameCenterAchievements=[String:GKAchievement]()
    
    

    
    
    
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController){
        gameCenterViewController.dismiss(animated: true, completion: nil)
        
        
    }
    
    func saveHighScore (number: Int){
        if GKLocalPlayer.localPlayer().isAuthenticated {
            let scoreChecker = GKScore(leaderboardIdentifier: "BestScoreRed")
            
            scoreChecker.value = Int64(number)
            
            let scoreArray: [GKScore] = [scoreChecker]
            
            GKScore.report(scoreArray, withCompletionHandler: nil)
            
            
        }
    }
    
    
    
    override func didMove(to view: SKView) {
        
        gameCenterLoadAchievements()
    
        
        scoreLabel = self.childNode(withName: "scoreLabel") as! SKLabelNode
       
        scoreLabel.text = "\(score)"
        
        
        
        starfield = SKEmitterNode(fileNamed: "Starfield")
        starfield.position = CGPoint(x: 0, y: 1472)
        starfield.advanceSimulationTime(10)
        self.addChild(starfield)
        
        starfield.zPosition = -1
        
        newGameButtonNode = self.childNode(withName: "newGameButton") as! SKSpriteNode
        newGameButtonNode.texture = SKTexture(imageNamed: "newGameButton")

       
        gameCenterNode = self.childNode(withName: "gameCenterButton") as! SKSpriteNode
        gameCenterNode.texture = SKTexture(imageNamed: "Leaderboard")
        
        if score <= 100 {
            
            addProgressToAnAchievement(progress: 100.0, achievementIdentifier: "RSNegAchieve")
            if isAchievementFinished(achievementIdentifier: "RSNegAchieve") == true {
                print("NegAchieve")
            }
        }
    
    }
 
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        for touch in touches {
            let location = touch.location(in: self)
            let node : SKNode = self.atPoint(location)
            if node.name == "newGameButton" {
                let transition = SKTransition.fade(withDuration: 0.5)
                let gameScene = GameScene(size: self.size)
                self.view!.presentScene(gameScene, transition: transition)
           
                
            } else if node.name == "gameCenterButton" {
                
                saveHighScore(number: score)
                
                print("clicking Game Center")
                let VC = self.view?.window?.rootViewController;
                
                let GCVC = GKGameCenterViewController()
                
                GCVC.gameCenterDelegate = self;
                
                VC?.present(GCVC, animated: true, completion: nil)
                
                
                
            }
                
            
            
        
        }
        
      
        
    }
    
    
    private func gameCenterLoadAchievements(){
        if canUseGameCenter == true {
            
            
            GKAchievement.loadAchievements(completionHandler: { (allAchievements, error) in
                if error != nil{
                    print("Game Center: could not load achievements, error: \(error)")
                } else {
                    for anAchievement in allAchievements!  {
                        if let oneAchievement = anAchievement as GKAchievement? {
                            self.gameCenterAchievements[oneAchievement.identifier!] = oneAchievement
                        }
                    }
                }
            })
            
            
            
        }
    }
    
    
    func isAchievementFinished(achievementIdentifier uAchievementId:String) -> Bool{
        if canUseGameCenter == true {
            let lookupAchievement:GKAchievement? = gameCenterAchievements[uAchievementId]
            if let achievement = lookupAchievement {
                if achievement.percentComplete == 100 {
                    return true
                }
            } else {
                gameCenterAchievements[uAchievementId] = GKAchievement(identifier: uAchievementId)
                return isAchievementFinished(achievementIdentifier: uAchievementId)
            }
        }
        return false
    }
    
    
    
    func addProgressToAnAchievement(progress uProgress:Double,achievementIdentifier uAchievementId: String) {
        if canUseGameCenter == true {
            let lookupAchievement:GKAchievement? = gameCenterAchievements[uAchievementId]
            
            if let achievement = lookupAchievement {
                if
                    achievement.percentComplete != 100 {
                    achievement.percentComplete = uProgress
                    
                    if uProgress == 100.0  {
                        /* show banner only if achievement is fully granted (progress is 100%) */
                        achievement.showsCompletionBanner=true
                    }
                    
                    
                    
                    GKAchievement.report([achievement], withCompletionHandler: { (error) in
                        if error != nil {
                            print("Couldn't save achievement (\(uAchievementId)) progress to \(uProgress) %")
                        }
                    })
                    
                    
                }
                
                
                
            } else {
                /* never added  progress for this achievement, create achievement now, recall to add progress */
                print("No achievement with ID (\(uAchievementId)) was found, no progress for this one was recoreded yet. Create achievement now.")
                gameCenterAchievements[uAchievementId] = GKAchievement(identifier: uAchievementId)
                /* recursive recall this func now that the achievement exist */
                addProgressToAnAchievement(progress: uProgress, achievementIdentifier: uAchievementId)
            }
        }
    }
    
    
    
    
    
    
    
    
    

    


    
}
