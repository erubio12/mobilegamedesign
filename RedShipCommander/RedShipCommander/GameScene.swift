//
//  GameScene.swift
//  RedShipCommander
//
//  Created by Erik Rubio on 1/14/17.
//  Copyright © 2017 Erik Rubio. All rights reserved.
//


import SpriteKit
import GameplayKit
import UIKit
import CoreMotion
import GameKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var starfield:SKEmitterNode!
    var player:SKSpriteNode!
    
    var canUseGameCenter:Bool = true {
        didSet {
            /* load prev. achievments form Game Center */
            if canUseGameCenter == true { gameCenterLoadAchievements() }
        }}
    /// Achievements of player
    var gameCenterAchievements=[String:GKAchievement]()
    

    
    var scoreLabel:SKLabelNode!
  
    
    var score:Int = 0 {
        didSet {
            scoreLabel.text = "Score: \(score)"
        
        }
    }
    
    var gameTimer:Timer!
    
    var aliens = ["greenAlien", "Alien2", "alien3"]
    
    let alienCategory:UInt32 = 0x1 << 1
    let photonTorpedoCategory:UInt32 = 0x1 << 0
    
    
    let motionManger = CMMotionManager()
    var xAcceleration:CGFloat = 0
    
    
    // Number of lives
    var livesArray:[SKSpriteNode]!
    
    override func didMove(to view: SKView) {
        
               gameCenterLoadAchievements()
        
        let music = SKAudioNode(fileNamed: "SpaceBackgroundMusic.mp3")
        addChild(music)
        
        music.isPositional = true
        music.position = CGPoint(x: -1024, y: 0)
        
        let moveForward = SKAction.moveTo(x: 1024, duration: 2)
        let moveBack = SKAction.moveTo(x: -1024, duration: 2)
        let sequence = SKAction.sequence([moveForward, moveBack])
        let repeatForever = SKAction.repeatForever(sequence)
        
        music.run(repeatForever)
        addLives()
        
        starfield = SKEmitterNode(fileNamed: "Starfield")
        starfield.position = CGPoint(x: 0, y: 1472)
        starfield.advanceSimulationTime(20)
        self.addChild(starfield)
        
        starfield.zPosition = -1
        
        player = SKSpriteNode(imageNamed: "FinalRedShip")
        
        player.position = CGPoint(x: self.frame.size.width / 2, y: player.size.height / 2 + 20)
        
        self.addChild(player)
        
        self.physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        self.physicsWorld.contactDelegate = self
        
        // Setting the scores stuff
        
        scoreLabel = SKLabelNode(text: "Score: 0")
        scoreLabel.position = CGPoint(x: 80, y: self.frame.size.height - 60)
        scoreLabel.fontName = "AmericanTypewriter-Bold"
        scoreLabel.fontSize = 28
        scoreLabel.fontColor = UIColor.white
        score = 0
        
      
       
        
        self.addChild(scoreLabel)
        //self.addChild(pauseLabel);
        
        var timeInterval = 0.75
        
        if UserDefaults.standard.bool(forKey: "hard") {
            timeInterval = 0.15
        }
        
        gameTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(addAlien), userInfo: nil, repeats: true)
        
        
        motionManger.accelerometerUpdateInterval = 0.2
        motionManger.startAccelerometerUpdates(to: OperationQueue.current!) { (data:CMAccelerometerData?, error:Error?) in
            if let accelerometerData = data {
                let acceleration = accelerometerData.acceleration
                self.xAcceleration = CGFloat(acceleration.x) * 0.75 + self.xAcceleration * 0.25
            }
        }
        
        
        
    }
    

    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
       laserAttack()
        
      
        
    }
    
    
    
    
    
    
   
    
    override func didSimulatePhysics() {
        
        player.position.x += xAcceleration * 50
        
        if player.position.x < -20 {
            player.position = CGPoint(x: self.size.width + 20, y: player.position.y)
        }else if player.position.x > self.size.width + 20 {
            player.position = CGPoint(x: -20, y: player.position.y)
        }
        
    }
    
    
    
    
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
       
        
        
        
    }
    
    
    // All Custom Functions
    
    func didBegin(_ contact: SKPhysicsContact) {
        var firstBody:SKPhysicsBody
        var secondBody:SKPhysicsBody
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        }else{
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if (firstBody.categoryBitMask & photonTorpedoCategory) != 0 && (secondBody.categoryBitMask & alienCategory) != 0 {
            destroyedAlien(torpedoNode: firstBody.node as? SKSpriteNode, alienNode: secondBody.node as? SKSpriteNode)
        }
        
    }
    
    
    func destroyedAlien (torpedoNode:SKSpriteNode?, alienNode:SKSpriteNode?) {
        
        if let explosion = SKEmitterNode(fileNamed: "Explosion") {
            
            if let alien = alienNode {
                explosion.position = alien.position
            }
            
            self.addChild(explosion)
            
            self.run(SKAction.playSoundFileNamed("explosion.mp3", waitForCompletion: false))
            
            if let torpedo = torpedoNode {
                torpedo.removeFromParent()
            }
            
            if let alien = alienNode {
                alien.removeFromParent()
            }
            
            
            
            self.run(SKAction.wait(forDuration: 2)) {
                explosion.removeFromParent()
            }
            
            score += 5
            
            if score == 1000 {
     
                addProgressToAnAchievement(progress: 100.0, achievementIdentifier: "RSAchieve")
                if isAchievementFinished(achievementIdentifier:"RSAchieve") == true {
                    print("Achievement completed")
                    
                }
            }
            if score == 250 {
         
                addProgressToAnAchievement(progress: 100.0, achievementIdentifier: "RSAchieve2")
                if isAchievementFinished(achievementIdentifier: "RSAchieve2") == true {
             
                }
            }
            
            if score == 500 {
                
                addProgressToAnAchievement(progress: 100.0, achievementIdentifier: "RSAchieve3")
                if isAchievementFinished(achievementIdentifier: "RSAchieve3") == true {
                  
                }
            }
            
            if score == 750 {
                
                addProgressToAnAchievement(progress: 100.0, achievementIdentifier: "RSAchieve4")
                if isAchievementFinished(achievementIdentifier: "RSAchieve4") == true {
                   
                }
            }
            
            if score == 150 {
                
                addProgressToAnAchievement(progress: 100.0, achievementIdentifier: "RSAchieve5")
                if isAchievementFinished(achievementIdentifier: "RSAchieve5") == true {
                    
                }
            }
            
            
            
        }
      
        
        
    }
    
   
    
    
    private func gameCenterLoadAchievements(){
        if canUseGameCenter == true {
      
            
            GKAchievement.loadAchievements(completionHandler: { (allAchievements, error) in
                if error != nil{
                    print("Game Center: could not load achievements, error: \(error)")
                } else {
                    for anAchievement in allAchievements!  {
                        if let oneAchievement = anAchievement as GKAchievement? {
                            self.gameCenterAchievements[oneAchievement.identifier!] = oneAchievement
                        }
                    }
                }
            })
            
            
             
        }
    }
        
    
    func isAchievementFinished(achievementIdentifier uAchievementId:String) -> Bool{
        if canUseGameCenter == true {
            let lookupAchievement:GKAchievement? = gameCenterAchievements[uAchievementId]
            if let achievement = lookupAchievement {
                if achievement.percentComplete == 100 {
                    return true
                }
            } else {
                gameCenterAchievements[uAchievementId] = GKAchievement(identifier: uAchievementId)
                return isAchievementFinished(achievementIdentifier: uAchievementId)
            }
        }
        return false
    }

    
    
    func addProgressToAnAchievement(progress uProgress:Double,achievementIdentifier uAchievementId: String) {
        if canUseGameCenter == true {
            let lookupAchievement:GKAchievement? = gameCenterAchievements[uAchievementId]
            
            if let achievement = lookupAchievement {
                if
                    achievement.percentComplete != 100 {
                    achievement.percentComplete = uProgress
                    
                    if uProgress == 100.0  {
                        /* show banner only if achievement is fully granted (progress is 100%) */
                        achievement.showsCompletionBanner=true
                    }
                    
                    
                    
                    GKAchievement.report([achievement], withCompletionHandler: { (error) in
                        if error != nil {
                            print("Couldn't save achievement (\(uAchievementId)) progress to \(uProgress) %")
                        }
                    })
                    
                
                }

                
                
            } else {
                /* never added  progress for this achievement, create achievement now, recall to add progress */
                print("No achievement with ID (\(uAchievementId)) was found, no progress for this one was recoreded yet. Create achievement now.")
                gameCenterAchievements[uAchievementId] = GKAchievement(identifier: uAchievementId)
                /* recursive recall this func now that the achievement exist */
                addProgressToAnAchievement(progress: uProgress, achievementIdentifier: uAchievementId)
            }
        }
    }
    
    
    
    
    

    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func addLives (){
        
        livesArray = [SKSpriteNode]()
        
        for live in 1 ... 3 {
            let liveNode = SKSpriteNode(imageNamed: "FinalRedShip")
            liveNode.name = "live\(live)"
            liveNode.position = CGPoint(x: self.frame.size.width - CGFloat((4 - live)) * liveNode.size.width, y: self.frame.size.height - 60)
            self.addChild(liveNode)
            livesArray.append(liveNode)
        }
    }
    
    func addAlien () {
        aliens = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: aliens) as! [String]
        
        let alien = SKSpriteNode(imageNamed: aliens[0])
        
        let randomAlienPosition = GKRandomDistribution(lowestValue: 0, highestValue: 414)
        let position = CGFloat(randomAlienPosition.nextInt())
        
        alien.position = CGPoint(x: position, y: self.frame.size.height + alien.size.height)
        
        alien.physicsBody = SKPhysicsBody(rectangleOf: alien.size)
        alien.physicsBody?.isDynamic = true
        
        alien.physicsBody?.categoryBitMask = alienCategory
        alien.physicsBody?.contactTestBitMask = photonTorpedoCategory
        alien.physicsBody?.collisionBitMask = 0
        
        self.addChild(alien)
        
        let animationDuration:TimeInterval = 6
        
        var actionArray = [SKAction]()
        
        
        actionArray.append(SKAction.move(to: CGPoint(x: position, y: -alien.size.height), duration: animationDuration))
        
        actionArray.append(SKAction.run {
            
            
            if self.livesArray.count > 0 {
                
                let liveNode = self.livesArray.first
                liveNode!.removeFromParent()
                self.livesArray.removeFirst()
                
                
                if self.livesArray.count == 0{
                    let transition = SKTransition.flipHorizontal(withDuration: 0.5)
                    let gameOver = SKScene(fileNamed: "GameOverScene") as! GameOverScene
                    gameOver.score = self.score
                    gameOver.scaleMode = .aspectFill
                    self.view?.presentScene(gameOver, transition: transition)
                }
                
            }
            
        })
        
        actionArray.append(SKAction.removeFromParent())
        
        alien.run(SKAction.sequence(actionArray))
        
        
    }
    func laserAttack() {
        self.run(SKAction.playSoundFileNamed("laser.mp3", waitForCompletion: false))
        
        let laserNode = SKSpriteNode(imageNamed: "RedLaser")
        laserNode.position = player.position
        laserNode.position.y += 5
        
        laserNode.physicsBody = SKPhysicsBody(circleOfRadius: laserNode.size.width / 2)
        laserNode.physicsBody?.isDynamic = true
        
        laserNode.physicsBody?.categoryBitMask = photonTorpedoCategory
        laserNode.physicsBody?.contactTestBitMask = alienCategory
        laserNode.physicsBody?.collisionBitMask = 0
        laserNode.physicsBody?.usesPreciseCollisionDetection = true
        
        self.addChild(laserNode)
        
        let animationDuration:TimeInterval = 0.3
        
        
        var actionArray = [SKAction]()
        
        actionArray.append(SKAction.move(to: CGPoint(x: player.position.x, y: self.frame.size.height + 10), duration: animationDuration))
        actionArray.append(SKAction.removeFromParent())
        
        laserNode.run(SKAction.sequence(actionArray))
        
        
        
    }
    
    
    
    
}
